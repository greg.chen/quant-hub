# ae_h - 2018/7/13

import json

import pandas as pd

from common_tools.decorators import exc_time, error_handler
from dao.futu_opend import futu_opend
from log.quant_logging import logger
from pitcher.context import Context
from pitcher.strategy import Strategy


class KDJStrategy(Strategy):
    def init(self, context):
        super(KDJStrategy, self).init(context)

        #context.pool = stock_pool_dao.get_list()['code'].values
        self.context = context
        self.context.pool = ["600196"]

    def fill_zero(self, code):
        code = str(code)
        code = code.zfill(6)
        return code

    @exc_time
    def handle_data(self):

        for code in self.context.pool:

            daily_stock_data = self.get_k_data(code=code, date=self.context.current_date)
            pre_daily_stock_data = self.get_k_data(code=code, date=self.context.pre_date)

            if daily_stock_data is None or pre_daily_stock_data is None:
                continue

            diff_value = daily_stock_data['diff']
            dea_value = daily_stock_data['dea']
            pre_diff_value = pre_daily_stock_data['diff']
            pre_dea_value = pre_daily_stock_data['dea']

            # 金叉
            if diff_value >= dea_value and pre_diff_value <= pre_dea_value:

                # basic_data = self.get_stock_basic(code=code)

                #if len(basic_data) <=0:
                    #continue

                last_close = daily_stock_data['close']
                self.buy_in_percent(code=code, price=last_close, percent=1)



            # target_frame.to_csv('kdj_result.csv')
            # 死叉
            if pre_diff_value > pre_dea_value and (diff_value <= dea_value):

                 position = self.context.get_position(code)

                 # 清仓
                 if position is not None and position.shares > 0:
                     self.sell_value(code, position.price, position.shares)


@error_handler()
def back():
    context = Context(start='2018-01-01', end='2018-07-01', base_capital=1000000)

    kdj = KDJStrategy('kdj')
    kdj.init(context)

    trade_days = []
    try:
        state, days =futu_opend.quote_ctx.get_trading_days(market='SH',start_date=context.start, end_date=context.end)
        trade_days = list(days)

    finally:
        futu_opend.close()

    kdj.pre_run()

    for date in trade_days:

        context.current_date = date
        if context.pre_date is None:
            context.pre_date = date
        else:
            kdj.before_handle_data()
            kdj.handle_data()
            context.pre_date = date
            kdj.after_handle_data()

    kdj.after_completion()

    #context_json = json.dumps(context, default=obj_dict)
    #logger.debug("context:" + context_json)

if __name__ == '__main__':
    back()
    #state,df = futu_opend.quote_ctx.get_history_kline('SH.600196', start='2018-01-15', end='2018-01-16')
    #print(df)