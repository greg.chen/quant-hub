import copy
import pandas as pd
from datetime import datetime

from common_tools.datetime_utils import get_next_date, DATE_FORMAT
from common_tools.decorators import exc_time
from common_tools.trade_fee_utlis import cal_commission_fee, cal_tax_fee
from dao.back_test.bt_order_dao import bt_order_dao
from dao.back_test.bt_position_dao import bt_position_dao
from dao.back_test.bt_profit_dao import bt_profit_dao
from dao.back_test.bt_summary_dao import bt_summary_dao
from dao.basic.stock_dao import stock_dao
from dao.k_data import fill_market
from dao.k_data.k_data_dao import k_data_dao
from domain.back_test.bt_order import BT_Order
from domain.back_test.bt_position import BT_Position
from domain.back_test.bt_profit import BT_Profit
from domain.back_test.bt_summary import BT_Summary
from feature_utils.custome_features import cal_mavol5, cal_mavol20
from feature_utils.momentum_indicators import acc_kdj, cal_macd
from feature_utils.overlaps_studies import cal_ma5, cal_ma10, cal_ma60, cal_ma20, cal_ma145


class Strategy:
    def __init__(self, strategy_code):
        self.strategy_code = strategy_code
        self.context = None

    def init(self, context):
        self.context = context

    def pre_run(self):

        stock_dbs = stock_dao.query_all()

        for stock in stock_dbs:
            self.context.stock_all[stock.code] = stock.name

        bt_order_dao.delete_by_strategy_code(strategy_code=self.strategy_code)
        bt_position_dao.delete_by_strategy_code(strategy_code=self.strategy_code)
        bt_profit_dao.delete_by_strategy_code(strategy_code=self.strategy_code)
        bt_summary_dao.delete_by_strategy_code(strategy_code=self.strategy_code)

        # 获取code pool的所有的k_data
        self.k_data_list = k_data_dao.get_multiple_k_data(code_list=self.context.pool,
                                                          start=get_next_date(days=-120, args=self.context.start),
                                                          end=get_next_date(days=120, args=self.context.end))

        self.k_data_list = self.k_data_list.set_index('code')

        self.k_data_dict = {}

        for code in self.context.pool:
            k_data = self.k_data_list.ix[fill_market(code)]
            k_data = k_data.reset_index(drop=True)

            feature_frame = pd.concat(
                [acc_kdj(k_data), cal_macd(k_data), cal_ma5(k_data), cal_ma10(k_data),
                 cal_ma20(k_data), cal_ma60(k_data), cal_ma145(k_data), cal_mavol5(k_data),
                 cal_mavol20(k_data)], axis=1)

            feature_frame.columns = ['k_value', 'd_value', 'j_value', 'macd', 'diff', 'dea', 'ma5', 'ma10', 'ma20',
                                     'ma60', 'ma145', 'mavol5',
                                     'mavol20']

            k_data = k_data.join(feature_frame)

            for index, row in k_data.iterrows():
                key = fill_market(code) + row["time_key"].strftime(DATE_FORMAT)
                self.k_data_dict[key] = row

    def after_completion(self):
        bt_position_dao.add_all(self.context.position_history_list)
        bt_profit_dao.add_all(self.context.profits)

        total_profit, profit_year = self.cal_total_profit(self.context.profits)

        hs300_profits = k_data_dao.get_hs300_profit(self.context.start, self.context.end)
        hs300_total_profit, hs300_profit_year = self.cal_total_profit(hs300_profits)

        summary = BT_Summary()
        summary.start_date = self.context.start
        summary.end_date = self.context.end
        summary.profit = total_profit
        summary.profit_year = profit_year
        summary.base_profit = hs300_total_profit
        summary.base_profit_year = hs300_profit_year
        summary.strategy_code = self.strategy_code
        summary.execute_time = datetime.now()
        bt_summary_dao.add(summary)


    # 计算总收益
    def cal_total_profit(self, profits):
        days_of_year = 250
        trade_days = len(profits)
        total_profit = profits[-1].profit_value

        profit_year = total_profit / trade_days * days_of_year

        return total_profit, profit_year

    def before_handle_data(self):

        if len(self.context.positions) == 0:
            # 记录当日收益
            self.context.profits.append(
                BT_Profit(self.strategy_code, self.context.current_date, (self.context.base_capital / self.context.init_capital - 1) * 100 ))
            return

        p_total = 0.0
        for position in self.context.positions:
            daily_stock_data = self.get_k_data(code=position.code, date=self.context.current_date)

            # 如果当日没有交易数据, 使用上一日的仓位数据填充
            if daily_stock_data is not None:
                price = daily_stock_data['close']
                position.price = price

            position.profit = round((position.price / position.price_in - 1) * 100, 2)
            position.worth = round(position.price * position.shares, 2)
            position.profit_value = round((position.price - position.price_in) * position.shares, 2)
            position.date_time = self.context.current_date
            total = position.price * position.shares
            p_total += total - self.cal_sell_trade_fee(total)

        # 计算净资产
        net_asset = p_total + self.context.blance
        profit = (net_asset / self.context.init_capital - 1) * 100

        # 记录当日收益
        self.context.profits.append(
            BT_Profit(self.strategy_code, self.context.current_date, profit))

    def after_handle_data(self):
        self.context.position_history_list.extend(copy.deepcopy(self.context.positions))

    # 买入
    # percent range is 0~1
    def buy_in_percent(self, code, price, percent):
        if percent > 1:
            raise ValueError('invalid percent')

        if price <= 0:
            raise ValueError('invalid price')
        # 计算最大购买金额
        amount = self.context.base_capital * percent
        # 如果账户余额不足, 退出
        if amount > self.context.blance:
            return

        # 计算购买股数
        shares = int(amount / price / 100) * 100

        # 购买金额
        total = shares * price

        # 交易费用(佣金)
        trade_fee = cal_commission_fee(self.context.commission_rate, total)

        # 如果总额加上佣金大于账户余额, 总股数减少100股, 再次计算总价格
        if total + trade_fee > amount:
            shares = shares - 100
            if shares <= 0:
                return
            # 交易费用(佣金)
            trade_fee = cal_commission_fee(self.context.commission_rate, total)

        # 账户余额扣除交易费用 + 交易金额
        self.context.blance -= (total + trade_fee)
        # 净资产扣除交易费用
        self.context.base_capital -= trade_fee

        self.add_position(code=code, name=self.context.stock_all[code], price=price, shares=shares, trade_fee=trade_fee)
        self.add_order_book(code=code, name=self.context.stock_all[code], action=1, price=price, shares=shares,
                            total=total,
                            date_time=self.context.current_date, trade_fee=trade_fee)

    # 卖出
    def sell_value(self, code, price, shares):

        position = self.context.get_position(code)
        total_shares = position.shares

        if shares < 100:
            raise ValueError('percent invalid')

        if shares > total_shares:
            raise ValueError('shares invalid')
        if shares == -1:
            shares = position.shares

        price_in = position.price_in

        # 卖出金额
        total = shares * price
        # 交易费用
        trade_fee = self.cal_sell_trade_fee(total)
        total = total - trade_fee

        self.context.blance += total

        profit = (price - price_in) * shares - trade_fee
        self.context.base_capital += profit

        # 如果全部卖出, 清空portfolio对应股票
        if shares == total_shares:
            self.context.delete_position(code)
        else:
            position.shares -= shares
            total = shares * price
            position.total -= total
            position.price_in = total / shares
            self.context.positions.append(position)

        # 添加卖出记录
        self.add_order_book(code=code, name=self.context.stock_all[code], action=0, price=price, shares=shares,
                            total=total,
                            date_time=self.context.current_date, trade_fee=trade_fee)

    def cal_sell_trade_fee(self, total):
        trade_fee = cal_tax_fee(self.context.tax_rate, total) + cal_commission_fee(self.context.commission_rate, total)
        return trade_fee

    def add_position(self, code, name, price, shares, trade_fee):

        position = self.context.get_position(code)

        if position is not None:
            # 计算平均价格
            position.price_in = (price * shares + position.shares * position.price_in) / (shares + position.shares)
            position.shares += shares
            position.worth += position.shares * position.price_in
            position.trade_fee += trade_fee

        else:
            # 新增投资组合
            position = BT_Position()

            position.code = code
            # 策略code
            position.strategy_code = self.strategy_code
            # 股票名称
            position.name = name
            # 当前价格
            position.price = price
            # 成本价
            position.price_in = price
            # 股数
            position.shares = shares
            # 盈亏
            position.profit = 0.0
            # 市值
            position.worth = shares * price
            # 盈亏金额
            position.profit_value = 0.0

            position.date_time = self.context.current_date
            # 记录投资组合
            self.context.positions.append(position)

        return position

    def add_order_book(self, code, name, action, price, shares, total, date_time, trade_fee):

        order = BT_Order()
        order.code = code
        order.name = name
        order.action = action
        order.price = price
        order.shares = shares
        order.total = total
        order.date_time = date_time
        order.trade_fee = trade_fee
        order.strategy_code = self.strategy_code
        bt_order_dao.add(order)

    @exc_time
    def get_k_data(self, code, date):

        try:
            key = fill_market(code) + date
            return self.k_data_dict[key]
        except:
            return None
        # k_data = self.k_data_list.query('code == "%s" and time_key == "%s" ' % (fill_market(code), self.context.current_date))
        '''
        start = convert_to_date(start)
        end = convert_to_date(end)

        try:
            k_data = self.k_data_list.ix[fill_market(code)]

            k_data = k_data.loc[(k_data['time_key'] >= start)
                                & (k_data['time_key'] <= end)]



            k_data = k_data.reset_index(drop=True)

            return k_data
        except:
            return None

        '''

    '''
    @exc_time
    def get_stock_basic(self, code):

        try:
            data = self.basic_data.ix[code]
            return data
        except:
            return None
    '''
