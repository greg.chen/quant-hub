# ae_h - 2018/9/6
from dao.basic.stock_pool_dao import stock_pool_dao
from pitcher.context import Context

class TbtStrategy(object):

    def init(self, context):

        context.pool = list(stock_pool_dao.get_list()['code'].values)

        self.context = context

    def handle_date(self):

        '''
        last close break through max(close in past 25)
        

        '''