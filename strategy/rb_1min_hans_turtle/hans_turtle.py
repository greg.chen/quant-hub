# ae_h - 2018/10/10
from datetime import *
from strategy.rb_1min_hans_turtle.ht_context import HTcontext
import pandas as pd
from feature_utils.volatility_indicators import cal_atr

# munging
rb05 = pd.read_csv("/Users/yw.h/quant-hub/strategy/rb_1min_hans_turtle/rb05.csv",
                   dtype={'open': float, 'high': float, 'low': float, 'close': float})

rb05['time'] = pd.to_datetime(rb05['time'])

rb05['date'] = rb05['time'].apply(lambda x: x.strftime('%Y-%m-%d'))
rb05['date'] = pd.to_datetime(rb05['date'])

rb05['timekey'] = rb05['time'].apply(lambda x: x.strftime('%H:%M:%S'))

rb05['timekey'] = rb05['timekey'].apply(lambda x: datetime.strptime(x, '%H:%M:%S').time())

rb05 = rb05.drop(['time'], axis=1)

temp_df = rb05
temp_df['timekey'] = temp_df['timekey'].apply(lambda x: x.strftime('%H:%M:%S'))

context = HTcontext()


def cal_unit_price(row):
    return (row['close'] * (1 + context.slide_point + context.trading_fee) * 10) * context.margin


def cal_loss_limit(row, temp_df, direction, index):
    if direction == 'L':
        context.atr_when_create = float('%.2f' % cal_atr(temp_df[:index])[-1:].values[0])
        context.loss_limit = row['high'] - 4 * context.atr_when_create
    if direction == 'S':
        context.atr_when_create = float('%.2f' % cal_atr(temp_df[:index])[-1:].values[0])
        context.loss_limit = row['high'] + 4 * context.atr_when_create


def cal_break_through_mark(temp_df, direction):
    if direction == 'L':
        return row['high'] + float('%.2f' % cal_atr(temp_df[:index])[-1:].values[0]) * 4
    if direction == 'S':
        return row['low'] - float('%.2f' % cal_atr(temp_df[:index])[-1:].values[0]) * 4


def create_unit(row, direction, temp_df, index):
    context.first_unit_price = cal_unit_price(row=row)

    context.base_capital -= context.first_unit_price

    if direction == 'L':
        print('----------create long----------')
        context.current_long_unit += 1
        context.market_price = row['close']

        # update damage control
        cal_loss_limit(row=row, temp_df=temp_df, direction='L', index=index)

        # update breakthrough
        context.long_break_through_mark = cal_break_through_mark(temp_df=temp_df, direction='L')

    if direction == 'S':
        print('----------create short----------')
        context.current_short_unit += 1
        context.market_price = row['close']
        # update damage control
        cal_loss_limit(row=row, temp_df=temp_df, direction='S', index=index)

        # update breakthrough
        context.short_break_through_mark = cal_break_through_mark(temp_df=temp_df, direction='S')


def add_unit(row, direction):
    context.second_unit_price = cal_unit_price(row=row)
    context.market_price = row['close']
    context.base_capital -= context.second_unit_price

    if direction == 'L':
        print('----------add long----------')
        context.current_long_unit += 1
        cal_loss_limit(row=row, temp_df=temp_df, direction='L', index=index)

    if direction == 'S':
        print('----------add short----------')
        context.current_short_unit += 1
        cal_loss_limit(row=row, temp_df=temp_df, direction='S', index=index)


def cal_profit(row):
    if context.current_long_unit == 2:
        context.total_profit += ((row['close'] - context.first_unit_price) + (
            row['close'] - context.second_unit_price)) * 10 * context.current_long_unit
    if context.current_long_unit == 1:
        context.total_profit += (row['close'] - context.first_unit_price) * 10
    if context.current_short_unit == 2:
        context.total_profit += ((context.first_unit_price - row['close']) + (
            context.second_unit_price - row['close'])) * 10 * context.current_short_unit
    if context.current_short_unit == 1:
        context.total_profit += (context.first_unit_price - row['close']) * 10


def cover_all(row, direction):
    if direction == 'L':
        print('----------cover long----------')
        context.market_price = row['close']
        cal_profit(row=row)
        context.base_capital += context.total_profit + row['close']
        context.current_long_unit = 0
        context.first_unit_price = 0
        context.second_unit_price = 0
        context.long_break_through_mark = 0
        context.short_break_through_mark = 0

    if direction == 'S':
        print('----------cover short----------')
        context.market_price = row['close']
        cal_profit(row=row)
        context.base_capital += context.total_profit + row['close']
        context.current_short_unit = 0
        context.first_unit_price = 0
        context.second_unit_price = 0
        context.long_break_through_mark = 0
        context.short_break_through_mark = 0


def reset_observe_interval():
    pass


def get_observe_interval():
    pass


def log_info():
    pass


def show_position(row):
    print('timekey:', row['date'], row['timekey'])

    print('current_long_pos:', context.current_long_unit)

    print('current_short_pos:', context.current_short_unit)

    print('base_capital:', float('%.2f' % context.base_capital))

    print('total_profit:', float('%.2f' % context.total_profit))

    print('market_price:', context.market_price)

    print('first_unit_price:', context.first_unit_price)

    print('second_unit_price', context.second_unit_price)

    print('create_loss_limit', context.loss_limit)

    print('ATR when open', context.atr_when_create)

    print('long_break_through_mark', float('%.2f' % context.long_break_through_mark))

    print('short_break_though_mark', float('%.2f' % context.short_break_through_mark))


# ((rb05['date'].values[345] - rb05['date'].values[344]).astype('timedelta64[D]').astype(int))


# sim on
for index, row in rb05[rb05['date'] == '2018-05-03'].iterrows():

    # cover
    if datetime.strptime(row['timekey'], '%H:%M:%S').time() == context.end_trading_evening:
        if context.current_long_unit != 0:
            cover_all(row=row, direction='L')
            show_position(row=row)
        if context.current_short_unit != 0:
            cover_all(row=row, direction='S')
            show_position(row=row)
        break
    if context.current_long_unit != 0 and row['close'] < context.loss_limit:
        cover_all(row=row, direction='L')
        show_position(row=row)
    if context.current_short_unit != 0 and row['close'] > context.loss_limit:
        cover_all(row=row, direction='S')
        show_position(row=row)

    # observe
    ob_upper = max(rb05[rb05['date'] == '2018-05-03']['high'].values[:20])
    ob_lower = min(rb05[rb05['date'] == '2018-05-03']['low'].values[:20])

    # create first unit
    # create long

    if row['high'] > ob_upper and context.current_long_unit == 0 and context.current_short_unit == 0:
        create_unit(row=row, direction='L', temp_df=temp_df, index=index)
        cal_loss_limit(row=row, temp_df=temp_df, index=index, direction='L')
        show_position(row=row)

    # create short
    if row['low'] < ob_lower and context.current_short_unit == 0 and context.current_long_unit == 0:
        create_unit(row=row, direction='S', temp_df=temp_df, index=index)
        cal_loss_limit(row=row, temp_df=temp_df, index=index, direction='S')
        show_position(row=row)

    # add unit
    if context.current_long_unit == 2 or context.current_short_unit == 2:
        continue
    if context.current_long_unit == 1 and row['high'] >= context.long_break_through_mark:
        add_unit(row=row, direction='L')
        show_position(row=row)
    if context.current_short_unit == 1 and row['low'] <= context.short_break_through_mark:
        add_unit(row=row, direction='S')
        show_position(row=row)
