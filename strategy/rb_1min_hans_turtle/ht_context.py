# ae_h - 2018/10/12
from datetime import *
import tushare as ts


class HTcontext():
    def __init__(self):
        # context
        self.base_capital = 100000

        self.max_unit = 2

        # current_position = 0

        self.current_long_unit = 0

        self.current_short_unit = 0

        self.first_unit_price = 0

        self.stop_mark = 0

        self.loss_limit = 0

        self.slide_point = 0.01

        self.observe_boundary = datetime.strptime('09:22:00', '%H:%M:%S').time()

        self.end_trading_noon = datetime.strptime('14:55:00', '%H:%M:%S').time()

        self.end_trading_evening = datetime.strptime('22:55:00', '%H:%M:%S').time()

        self.total_profit = 0

        self.freq_rate = 0

        self.trading_fee_rate = 0.0002

        self.margin = 0.1

        self.long_break_through_mark = 0

        self.short_break_through_mark = 0

        self.second_unit_price = 0

        self.market_price = 0

        self.atr_when_create = 0

        self.trading_fee = 0



