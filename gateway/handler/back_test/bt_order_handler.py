from dao.back_test.bt_order_dao import bt_order_dao

from gateway.common.base_handler import BaseHandler
from gateway.errors import ResourceNotFoundException


class BT_OrderHandler(BaseHandler):
    """
    Handle for endpoint: /bt/order/{strategy_code}
    """

    def on_get(self, req, resp, strategy_code):

        order_dbs = bt_order_dao.query_by_strategy_code(strategy_code)

        self.on_success(resp=resp, data=order_dbs)

