from dao.back_test.bt_position_dao import bt_position_dao

from gateway.common.base_handler import BaseHandler


class BT_PositionHandler(BaseHandler):
    """
    Handle for endpoint: /bt/position/{strategy_code}
    """

    def on_get(self, req, resp, strategy_code):

        position_dbs = bt_position_dao.query_by_strategy_code(strategy_code)

        self.on_success(resp=resp, data=position_dbs)

