from dao.back_test.bt_profit_dao import bt_profit_dao
from dao.back_test.bt_summary_dao import bt_summary_dao
from dao.k_data.k_data_dao import k_data_dao

from gateway.common.base_handler import BaseHandler
from gateway.errors import ResourceNotFoundException


class BT_ProfitHandler(BaseHandler):
    """
    Handle for endpoint: /bt/profit/{strategy_code}
    """

    def on_get(self, req, resp, strategy_code):

        profit_dbs = bt_profit_dao.query_by_strategy_code(strategy_code)

        if profit_dbs is None:
            raise ResourceNotFoundException("Can not found summary list.")

        self.on_success(resp=resp, data=profit_dbs)



class BT_Profit_HS300_Handler(BaseHandler):
    """
    Handle for endpoint: /bt/profit/hs300
    """

    def on_post(self, req, resp):

        req = req.context['data']

        profit_dbs = k_data_dao.get_hs300_profit(start=req['start_date'], end=req['end_date'])

        self.on_success(resp=resp, data=profit_dbs)
