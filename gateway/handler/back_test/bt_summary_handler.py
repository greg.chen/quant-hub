from dao.back_test.bt_summary_dao import bt_summary_dao
from dao.trade.strategy_dao import strategy_dao
from gateway.common.base_handler import BaseHandler
from gateway.errors import ResourceNotFoundException


class BT_SummarySearchHandler(BaseHandler):
    """
    Handle for endpoint: /bt/summary/search
    """

    def on_post(self, req, resp):

        summary_dbs = bt_summary_dao.query_all()

        if summary_dbs is None:
            raise ResourceNotFoundException("Can not found summary list.")

        strategy_dbs = strategy_dao.query_all()
        group = {"list": []}
        for strategy in strategy_dbs:
            summary_list = [t.to_dict() for t in summary_dbs if t.strategy_code == strategy.code]

            group_item = {"strategy_code": strategy.code, "strategy_name": strategy.name,
                          "summary_list": summary_list}

            group["list"].append(group_item)

        self.on_success(resp=resp, data=group)



