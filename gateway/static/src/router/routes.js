import Main from '@view/Main'
import TargetMng from '@view/TargetMng'
import PositionMng from '@view/PositionMng'
import Backtest from '@view/Backtest'
import BacktestDetail from '@view/BacktestDetail'

export default [
  {
    path: '/',
    name: 'main',
    component: Main,
    redirect: '/target/mng',
    children: [
      {
        path: '/target/mng',
        name: 'TargetMng',
        menu: true,
        component: TargetMng
      }, {
        path: '/position/mng',
        name: 'PositionMng',
        menu: true,
        component: PositionMng
      }, {
        path: '/backtest/mng',
        name: 'Backtest',
        menu: true,
        component: Backtest
      }, {
        path: '/backtest/:id',
        name: 'BacktestDetail',
        component: BacktestDetail
      }
    ]
  }, {
    path: '*',
    redirect: '/target/mng'
  }
]
