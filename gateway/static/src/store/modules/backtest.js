import publicApi from '@api/public'
const state = {
  strategyList: {}
}
const getters = {
  status: state => state.status
}
Object.keys(state).forEach(key => {
  getters[key] = state => state[key]
})
const actions = {
  async getStrategySearch ({commit}, params = {}) {
    try {
      const {data = []} = await publicApi.strategySearch(params)
      let list = data || []
      list.forEach(item => {
        item.text = item.name
        item.value = item.code
      })
      commit('SET_STATE', {target: 'strategyList', data: list})
    } catch (error) {
      throw error
    }
  }
}
const mutations = {
  SET_STATE (state, payload) {
    const { target, data } = payload
    state[target] = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
