import axios from './'
export default {
  stockCode: params => axios.get(`api/stock/${params.code}`, params),
  strategySearch: params => axios.post('/api/strategy/search', params)
}
