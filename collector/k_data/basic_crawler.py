#  -*- coding: utf-8 -*-
import traceback
from datetime import datetime, timedelta

import tushare as ts
from pymongo import UpdateOne

from dao.mongo_data_source import MONGO_DB_CONN
from dao.basic.stock_util import get_trading_dates
from log.quant_logging import logger

"""
从tushare获取股票基础数据，保存到本地的MongoDB数据库中
"""

def crawl_basic(begin_date=None, end_date=None):
    """
    抓取指定时间范围内的股票基础信息
    :param begin_date: 开始日期
    :param end_date: 结束日期
    """

    if begin_date is None:
        begin_date = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')

    if end_date is None:
        end_date = (datetime.now() - timedelta(days=1)).strftime('%Y-%m-%d')

    all_dates = get_trading_dates(begin_date, end_date)

    for date in all_dates:
        try:
            crawl_basic_at_date(date)
        except:
            logger.error('抓取股票基本信息时出错，日期：%s, error: %s' % (date, traceback.format_exc()))


def crawl_basic_at_date(date):
    """
    从Tushare抓取指定日期的股票基本信息
    :param date: 日期
    """
    # 默认推送上一个交易日的数据
    df_basics = ts.get_stock_basics(date)

    # 如果当日没有基础信息，在不做操作
    if df_basics is None:
        return

    update_requests = []
    codes = set(df_basics.index)

    for code in codes:
        doc = dict(df_basics.loc[code])
        try:

            if str(doc['timeToMarket']) == '0':
                continue

            # 将20180101转换为2018-01-01的形式
            time_to_market = datetime \
                .strptime(str(doc['timeToMarket']), '%Y%m%d') \
                .strftime('%Y-%m-%d')

            totals = float(doc['totals'])
            outstanding = float(doc['outstanding'])

            doc.update({
                'code': code,
                'date': date,
                'timeToMarket': time_to_market,
                'outstanding': outstanding,
                'totals': totals
            })

            update_requests.append(
                UpdateOne(
                    {'code': code, 'date': date},
                    {'$set': doc}, upsert=True))

        except:
            logger.error('发生异常，股票代码：%s，日期：%s, error: %s' % (code, date, traceback.format_exc()))

    if len(update_requests) > 0:
        update_result = MONGO_DB_CONN['basic'].bulk_write(update_requests, ordered=False)

        logger.debug('抓取股票基本信息，日期：%s, 插入：%4d条，更新：%4d条' % (date, update_result.upserted_count, update_result.modified_count))

if __name__ == '__main__':
    crawl_basic()