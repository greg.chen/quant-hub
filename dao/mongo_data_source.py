from pymongo import MongoClient, ASCENDING, DESCENDING, UpdateOne

from config import default_config

MONGO_DB_CONN = MongoClient(default_config.MONGODB_URI)['quant']