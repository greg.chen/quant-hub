from common_tools.decorators import exc_time
from dao import dataSource


class Base_Dao():
    @exc_time
    def add(self, obj):
        with dataSource.session_ctx() as session:
            session.add(obj)

    @exc_time
    def add_all(self, objs):
        with dataSource.session_ctx() as session:
            session.bulk_save_objects(objs)

    @exc_time
    def update(self, obj):
        with dataSource.session_ctx() as session:
            session.merge(obj)

    @exc_time
    def delete(self, obj):
        with dataSource.session_ctx() as session:
            session.delete(obj)