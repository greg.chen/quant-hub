import copy

from common_tools.decorators import exc_time
from dao import dataSource
from dao.base_dao import Base_Dao
from domain.back_test.bt_summary import BT_Summary


class BT_Summary_Dao(Base_Dao):

    @exc_time
    def query_by_strategy_code(self, strategy_code):
        with dataSource.session_ctx() as session:
            position_dbs = session.query(BT_Summary).filter(BT_Summary.strategy_code == strategy_code).first()
            return copy.deepcopy(position_dbs)

    @exc_time
    def delete_by_strategy_code(self, strategy_code):
        with dataSource.session_ctx() as session:
            position_dbs = session.query(BT_Summary).filter(BT_Summary.strategy_code == strategy_code).delete()

            return copy.deepcopy(position_dbs)

    @exc_time
    def query_all(self):
        with dataSource.session_ctx() as session:
            position_dbs = session.query(BT_Summary).all()

            return copy.deepcopy(position_dbs)




bt_summary_dao = BT_Summary_Dao()
