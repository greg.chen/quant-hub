import copy

from common_tools.decorators import exc_time
from dao import dataSource
from domain.back_test.bt_order import BT_Order


class BT_Order_Dao():
    @exc_time
    def query_by_strategy_code(self, strategy_code):
        with dataSource.session_ctx() as session:
            order_dbs = session.query(BT_Order).filter(BT_Order.strategy_code == strategy_code).all()

            return copy.deepcopy(order_dbs)

    @exc_time
    def delete_by_strategy_code(self, strategy_code):
        with dataSource.session_ctx() as session:
            order_dbs = session.query(BT_Order).filter(BT_Order.strategy_code == strategy_code).delete()

            return copy.deepcopy(order_dbs)

    @exc_time
    def query_all(self):
        with dataSource.session_ctx() as session:
            order_dbs = session.query(BT_Order).all()

            return copy.deepcopy(order_dbs)

    @exc_time
    def add(self, order):
        with dataSource.session_ctx() as session:
            session.add(order)

    @exc_time
    def update(self, order):
        with dataSource.session_ctx() as session:
            session.merge(order)

    @exc_time
    def delete(self, order):
        with dataSource.session_ctx() as session:
            session.delete(order)


bt_order_dao = BT_Order_Dao()
