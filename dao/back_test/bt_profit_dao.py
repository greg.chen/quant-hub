import copy

from common_tools.decorators import exc_time
from dao import dataSource
from domain.back_test.bt_profit import BT_Profit


class BT_Profit_Dao():

    @exc_time
    def query_by_strategy_code(self, strategy_code):
        with dataSource.session_ctx() as session:
            position_dbs = session.query(BT_Profit).filter(BT_Profit.strategy_code == strategy_code).all()
            return copy.deepcopy(position_dbs)

    @exc_time
    def delete_by_strategy_code(self, strategy_code):
        with dataSource.session_ctx() as session:
            position_dbs = session.query(BT_Profit).filter(BT_Profit.strategy_code == strategy_code).delete()

            return copy.deepcopy(position_dbs)

    @exc_time
    def query_all(self):
        with dataSource.session_ctx() as session:
            position_dbs = session.query(BT_Profit).all()

            return copy.deepcopy(position_dbs)

    @exc_time
    def add(self, profit):
        with dataSource.session_ctx() as session:
            session.add(profit)

    @exc_time
    def add_all(self, profits):
        with dataSource.session_ctx() as session:
            session.bulk_save_objects(profits)

    @exc_time
    def update(self, profit):
        with dataSource.session_ctx() as session:
            session.merge(profit)

    @exc_time
    def delete(self, profit):
        with dataSource.session_ctx() as session:
            session.delete(profit)


bt_profit_dao = BT_Profit_Dao()
