import copy

from common_tools.decorators import exc_time
from dao import dataSource
from domain.back_test.bt_position import BT_Position


class BT_Position_Dao():
    @exc_time
    def query_by_strategy_code(self, strategy_code):
        with dataSource.session_ctx() as session:
            position_dbs = session.query(BT_Position).filter(BT_Position.strategy_code == strategy_code).all()

            return copy.deepcopy(position_dbs)

    @exc_time
    def delete_by_strategy_code(self, strategy_code):
        with dataSource.session_ctx() as session:
            position_dbs = session.query(BT_Position).filter(BT_Position.strategy_code == strategy_code).delete()

            return copy.deepcopy(position_dbs)

    @exc_time
    def query_all(self):
        with dataSource.session_ctx() as session:
            position_dbs = session.query(BT_Position).all()

            return copy.deepcopy(position_dbs)

    @exc_time
    def add(self, position):
        with dataSource.session_ctx() as session:
            session.add(position)

    @exc_time
    def add_all(self, positions):
        with dataSource.session_ctx() as session:
            session.bulk_save_objects(positions)

    @exc_time
    def update(self, position):
        with dataSource.session_ctx() as session:
            session.merge(position)

    @exc_time
    def delete(self, position):
        with dataSource.session_ctx() as session:
            session.delete(position)


bt_position_dao = BT_Position_Dao()
