from sqlalchemy import Column, String, Integer, Float, DateTime, BigInteger
from sqlalchemy.ext.hybrid import hybrid_property

from domain.base import Base

'''
    仓位类
'''


class Position(Base):
    # 股票代码
    id = Column(BigInteger, primary_key=True)
    # 股票代码
    code = Column(String)
    # 策略code
    strategy_code = Column(String)
    # 股票名称
    name = Column(String)
    # 当前价格
    price = Column(Float)
    # 成本价
    price_in = Column(Float)
    # 股数
    shares = Column(Integer)
    # 盈亏
    profit = Column(Float)
    # 市值
    worth = Column(Float)
    # 盈亏金额
    profit_value = Column(Float)

