from sqlalchemy import Column, Integer, String, Float, BigInteger, DateTime

from domain.base import Base


class BT_Order(Base):
    id = Column(BigInteger, primary_key=True)

    # 股票代码
    code = Column(String)

    name = Column(String)
    # 策略code
    strategy_code = Column(String)

    # 1-买入, 0-卖出
    action = Column(Integer)
    # shares
    shares = Column(Integer)
    # 买入价格
    price = Column(Float)
    # 总价
    total = Column(Float)
    # 交易费用
    trade_fee = Column(Float)
    # 操作时间
    date_time = Column(DateTime)

