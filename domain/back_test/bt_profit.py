
from sqlalchemy import Column, BigInteger, Float, String, DateTime

from domain.base import Base


class BT_Profit(Base):
    id = Column(BigInteger, primary_key=True)
    # 策略code
    strategy_code = Column(String)

    # 收益
    profit_value = Column(Float)

    # 操作时间
    date_time = Column(DateTime)

    def __init__(self, strategy_code,date_time, value):
        self.strategy_code = strategy_code
        self.date_time = date_time
        self.profit_value = value