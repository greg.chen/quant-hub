
from sqlalchemy import Column, BigInteger, Float, String, DateTime

from domain.base import Base


class BT_Summary(Base):
    id = Column(BigInteger, primary_key=True)
    # 策略code
    strategy_code = Column(String)

    # 收益
    profit= Column(Float)

    # 年化收益
    profit_year = Column(Float)

    # 基准收益
    base_profit = Column(Float)

    # 基准年化收益
    base_profit_year = Column(Float)

    # 执行时间
    execute_time = Column(DateTime)

    # 开始时间
    start_date = Column(DateTime)

    # 结束时间
    end_date = Column(DateTime)