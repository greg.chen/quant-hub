
from sqlalchemy import Column, BigInteger, Float, Integer, String, DateTime

from domain.base import Base


class BT_Position(Base):
    id = Column(BigInteger, primary_key=True)
    # 股票代码
    code = Column(String)
    # 策略code
    strategy_code = Column(String)
    # 股票名称
    name = Column(String)
    # 当前价格
    price = Column(Float)
    # 成本价
    price_in = Column(Float)
    # 股数
    shares = Column(Integer)
    # 盈亏
    profit = Column(Float)
    # 市值
    worth = Column(Float)
    # 盈亏金额
    profit_value = Column(Float)

    # 操作时间
    date_time = Column(DateTime)