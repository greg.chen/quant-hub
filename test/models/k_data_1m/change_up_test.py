# ae_h - 2018/8/12
import unittest

from sklearn import svm
from sklearn.metrics import r2_score

from dao.futu_opend import futu_opend
from feature_utils.momentum_indicators import cal_macd, acc_kdj
import pandas as pd
from sklearn.model_selection import train_test_split, GridSearchCV


class ChangeUpTest(unittest.TestCase):
    def test_training(self):
        status, df = futu_opend.quote_ctx.get_history_kline(code='SH.511880', start='2017-07-01', end='2018-07-20',
                                                            ktype='K_1M')
        macd = cal_macd(df)
        df = pd.concat([df, macd], axis=1)

        k_d_j = acc_kdj(df)
        df = pd.concat([df, k_d_j], axis=1)

        # features = ['open', 'high', 'low', 'turnover_rate', 'volume', 'change_rate', 'macd', 'diff', 'dea', 'k_value',
        #             'd_value', 'j_value']


        df = df.dropna()

        X = df[['open', 'high', 'last_close']]

        y = df['close']

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.3, shuffle=False, random_state=10)

        tuned_parameters = [
            {'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]}
        ]

        svr_model = GridSearchCV(svm.SVR(), tuned_parameters, n_jobs=-1)

        svr_model.fit(X_train, y_train)

        y_pred = svr_model.predict(X_test)

        pred_score = r2_score(y_test, y_pred)

        print(pred_score)

    def tearDown(self):
        futu_opend.quote_ctx.close()
