# ae_h - 2018/9/9

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


def autoNorm(data):  # 传入一个矩阵
    mins = data.min(0)  # 返回data矩阵中每一列中最小的元素，返回一个列表
    maxs = data.max(0)  # 返回data矩阵中每一列中最大的元素，返回一个列表
    ranges = maxs - mins  # 最大值列表 - 最小值列表 = 差值列表
    normData = np.zeros(np.shape(data))  # 生成一个与 data矩阵同规格的normData全0矩阵，用于装归一化后的数据
    row = data.shape[0]  # 返回 data矩阵的行数
    normData = data - np.tile(mins, (row, 1))  # data矩阵每一列数据都减去每一列的最小值
    normData = normData / np.tile(ranges, (row, 1))  # data矩阵每一列数据都除去每一列的差值（差值 = 某列的最大值- 某列最小值）
    return normData


sns.set_style('whitegrid')

df3 = pd.read_csv('/Users/yw.h/Downloads/3rd.csv')
df4 = pd.read_csv('/Users/yw.h/Downloads/4th.csv')

df3_to_norm = df3.loc[:, ['Pro', 'Elite', 'we Media', 'Host/Celebrity']]
df4_to_norm = df4.loc[:, ['Pro', 'Elite', 'we Media', 'Host/Celebrity']]
norm_df3 = autoNorm(df3_to_norm)
norm_df4 = autoNorm(df4_to_norm)

norm_df3['Category'] = df3['Category']
norm_df4['Category'] = df4['Category']



f, (ax1, ax2) = plt.subplots(figsize=(6, 4), nrows=2)

# cmap = sns.cubehelix_palette(start=1.5, rot=3, gamma=0.8, as_cmap=True)

norm_df3 = norm_df3.set_index('Category')
norm_df4 = norm_df4.set_index('Category')

print(norm_df3)
print(norm_df4)

sns.heatmap(norm_df3, linewidths=0.05, ax=ax1, annot=True, cmap='RdBu_r', center=0.2)
ax1.set_title('Subscription')

sns.heatmap(norm_df4, linewidths=0.05, ax=ax2, annot=True, cmap='RdBu_r', center=0.2)
ax2.set_title('Q&A')
ax2.set_xlabel('Producer Type')

plt.legend()
plt.show()
