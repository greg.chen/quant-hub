# coding=utf-8
# ae_h - 2018/9/9


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_style('whitegrid')

df1 = pd.read_csv('/Users/yw.h/Downloads/1st.csv')
df2 = pd.read_csv('/Users/yw.h/Downloads/2nd.csv')

f, (ax1, ax2) = plt.subplots(figsize=(6, 4), nrows=2)

df1 = df1.set_index('Category')
df2 = df2.set_index('Category')


sns.barplot(df1.index.tolist(), df1['Total'], ax=ax1, palette='Set3')
ax1.set_title('Top 4 Lessons')

sns.set_style('whitegrid')
sns.barplot(df2.index.tolist(), df2['Total'], ax=ax2, palette='Set3')
ax2.set_title('Sub-Category under "Skill Learning"')


plt.legend()
plt.show()
