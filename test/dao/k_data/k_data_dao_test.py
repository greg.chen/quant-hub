import unittest

from common_tools.datetime_utils import get_next_date, get_current_date
from config import default_config
import futuquant as ft
from dao.k_data.k_data_dao import k_data_dao
from feature_utils.momentum_indicators import cal_macd


class K_Data_Dao_Test(unittest.TestCase):
    def setUp(self):
        # before_run()
        self.futu_quote_ctx = ft.OpenQuoteContext(host=default_config.FUTU_OPEND_HOST, port=default_config.FUTU_OPEND_PORT)

    def test_get_k_data(self):

        df = k_data_dao.get_k_data(code = "600196", start="2018-01-02", end="2018-01-06")
        print(df)
        self.assertIsNotNone(df)

    def get_multiple_k_data(self):

        df = k_data_dao.get_multiple_k_data(code_list = ["601398", '600196', '000001'], start="2018-07-02", end="2018-07-28")
        print(df)
        self.assertIsNotNone(df)


    def test_get_trading_days(self):
        df = k_data_dao.get_trading_days(start="2015-01-01", end="2018-05-27", futu_quote_ctx= self.futu_quote_ctx)
        print(df)
        self.assertIsNotNone(df)

    def test_get_market_snapshot(self):
        df = k_data_dao.get_market_snapshot(code_list=['600196', '601398'], futu_quote_ctx= self.futu_quote_ctx)
        print(df)

    def test_get_last_macd_cross_point(self):
        df = k_data_dao.get_k_data(code = "601668", start=get_next_date(-120), end=get_current_date())
        df = df.join(cal_macd(df))
        item = k_data_dao.get_last_macd_cross_point(df, window_size=8)

        print(item)

    def tearDown(self):
        self.futu_quote_ctx.close()

