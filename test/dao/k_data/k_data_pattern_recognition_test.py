import unittest

from datetime import datetime

from common_tools.datetime_utils import convert_to_datetime, convert_to_date
from dao.k_data.k_data_dao import k_data_dao
import pandas  as pd
import matplotlib.pyplot as plt


class K_Data_Pattern_Recognition_Test(unittest.TestCase):
    """
        检测出这些局部转折点
    """

    def test_k_data_turning_point(self):
        data = k_data_dao.get_k_data('SZ.000852', start='2013-01-01', end='2013-10-29')


        sdn = self.cal_turning_down_points(data)
        sup = self.cal_turning_up_point(data)
        print(self.is_up_channel(sdn, sup, data['close'].values[-1]))



        # print(sdn)
        # print(sdn['close'].rolling(3).max())
        # print(sup[['time_key','close']])



        print(sdn[['time_key', 'close']].tail(3))
        print(sup[['time_key', 'close']].tail(3))



        # splot = self.filter_turning_point(sall)

        # fig, ax = plt.subplots()
        # ax.plot(sall['time_key'], sall['close'], label='all')
        # ax.plot(sup['time_key'], sup['close'], label='sup')
        # plt.show()

        # print(sall)

    def is_up_channel(self, sdn, sup, price):

        if price > sdn['high'].values[-1] and price > sdn['high'].values[-2] and price > sdn['high'].values[-3]:
            return True

        down = sdn['high'].values[-1] > sdn['high'].values[-2] and sdn['high'].values[-1] >  sdn['high'].values[-3]
        up = sup['low'].values[-1] > sup['low'].values[-2] and sup['low'].values[-1] >  sup['low'].values[-3]

        rs = down and up
        return rs

    def cal_turning_down_points(self, data, frequency=3):

        sdn = pd.DataFrame(columns=data.columns)
        count = len(data.index)
        # 查找所有turning down points
        for index, row in data.iterrows():
            #
            if index == 0:
                continue

            if count == index + 1:
                continue

            pre_row = data.iloc[index - 1]
            row = data.iloc[index]
            next_row = data.iloc[index + 1]

            if row['high'] >= pre_row['high'] and row['high'] > next_row['high']:
                sdn = sdn.append(row)

        sdn = sdn.reset_index()

        rs = pd.DataFrame(columns=data.columns)

        sdn_count = len(sdn.index)
        last_round = False

        # 3个一组, 过滤出最大值.
        for i in range(0, sdn_count, frequency):

            if i + frequency * 2 > sdn_count:
                step = sdn_count - i
                last_round = True
            else:
                step = frequency

            max_row = None
            for j in range(0, step):

                row = sdn.iloc[i + j]

                if max_row is None:
                    max_row = row
                    continue

                if max_row['high'] < row['high']:
                    max_row = row

            rs = rs.append(max_row)

            if last_round:
                break

        return rs

    def cal_turning_up_point(self, data, frequency=3):


        sup = pd.DataFrame(columns=data.columns)
        count = len(data.index)

        # 查找所有turning up points
        for index, row in data.iterrows():
            #
            if index == 0:
                continue

            if count == index + 1:
                continue

            pre_row = data.iloc[index - 1]
            row = data.iloc[index]
            next_row = data.iloc[index + 1]

            if row['low'] <= pre_row['low'] and row['low'] < next_row['low']:
                sup = sup.append(row)

        sup = sup.reset_index()
        # 3个一组, 过滤出最小值.
        rs = pd.DataFrame(columns=data.columns)
        sup_count = len(sup.index)
        last_round = False
        for i in range(0, sup_count, frequency):

            if i + frequency * 2 > sup_count:
                step = sup_count - i
                last_round = True
            else:
                step = frequency

            min_row = None
            for j in range(0, step):

                row = sup.iloc[i + j]

                if min_row is None:
                    min_row = row
                    continue

                if min_row['low'] > row['low']:
                    min_row = row

            rs = rs.append(min_row)

            if last_round:
                break

        return rs
