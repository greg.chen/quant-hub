import unittest

from dao.back_test.bt_order_dao import bt_order_dao
from dao.basic.stock_dao import stock_dao
from domain.back_test.bt_order import BT_Order


class BT_Order_Test(unittest.TestCase):

    def test_query_query_all(self):
        order = bt_order_dao.query_all()

        print(order)

    def test_all(self):

        order = BT_Order()
        order.code = '600196'
        order.strategy_code = 'macd'
        order.price = 33
        bt_order_dao.add(order)

        print(order)