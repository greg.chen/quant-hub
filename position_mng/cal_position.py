# ae_h - 2018/9/8
from common_tools.datetime_utils import get_next_date, get_current_date
from feature_utils.volatility_indicators import cal_atr
from dao.k_data.k_data_dao import k_data_dao
import tushare as ts
import pandas as pd
from common_tools.decorators import exc_time

base_capital = 1000000
risk_rate = 0.01


def cal_first_position():
    k_data = k_data_dao.get_k_data(code='600036', start=get_next_date(-100), end=get_current_date())

    k_data['atr'] = cal_atr(k_data)

    print(k_data)

    cur_price = k_data['close'].values[-1]
    cur_atr = k_data['atr'].values[-1]

    amout_per_atr = (cur_price * cur_atr * 100)

    first_position = (base_capital * risk_rate) / amout_per_atr

    print(amout_per_atr)
    print(first_position)


@exc_time
def filter_highest():
    ts.set_token('a8804bf48cae338e18cc1289e3915696b6d439d80d632b5ead5cc3b6')

    pro = ts.pro_api()

    df = pro.daily(ts_code='000001.SZ', start_date='20170801', end_date='20180718')

    for i in range(0, len(df.index)):
        cur_high = df.iloc[i]['high']

        if i <= 6 or i >= len(df.index-6):
            continue

        if cur_high >= max(df.iloc[i-5:i]['high']) and cur_high >= max(df.iloc[i:i+6]['high']):
            print(df.iloc[i])



filter_highest()
